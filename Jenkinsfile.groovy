node() {
    try {
        stage('PremiereEtape') {
            sh "echo 'Hello World !!'"
        }
        stage('DeuxiemeEtape') {
            sh "echo 'Hello World !!'"
        }
    }
    catch (e) {
        // if any exception occurs, mark the build as failed
        currentBuild.result = 'FAILURE'
        throw e
    }
    finally {
        cleanWs cleanWhenFailure: false
    }
}
